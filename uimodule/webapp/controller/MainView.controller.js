sap.ui.define(
    [
        "com/mknaf/nonograms/controller/BaseController", //
        "com/mknaf/nonograms/model/models",
    ],

    function (
        Controller, //
        models
    ) {
        "use strict";

        return Controller.extend("com.mknaf.nonograms.controller.MainView", {
            onInit: function (oEvent) {
                this.setModel(models.createViewModel(), "view");

                // DEBUG
                this._initRandomNonogram();
                window.oController = this;

                return oEvent;
            },

            onNewNonogramPress: function (oEvent) {
                const oViewModel = this.getView().getModel("view");

                oViewModel.setProperty("/bShowSolution", false);

                this._initRandomNonogram();

                return oEvent;
            },

            _initRandomNonogram: function () {
                const oViewModel = this.getView().getModel("view");
                const x = oViewModel.getProperty("/x");
                const y = oViewModel.getProperty("/y");

                // randomly check some cells
                const n = [];
                for (let i = 0; i < x; i++) {
                    const row = [];
                    for (let j = 0; j < y; j++) {
                        const iValue = Math.random() < 0.5 ? 0 : 1;
                        row.push({
                            iValue: iValue,
                            bPressed: false, // !!iValue,
                        });
                    }
                    n.push({ cells: row });
                }

                oViewModel.setProperty("/rows", n);

                this._updateRowHeaders();
                this._updateCellHeaders();
            },

            _updateRowHeaders: function () {
                const oViewModel = this.getView().getModel("view");
                const iSizeX = oViewModel.getProperty("/x");
                const iSizeY = oViewModel.getProperty("/y");

                // for all rows
                for (let row = 0; row < iSizeX; row++) {
                    let i = 0;
                    const aTmp = [];
                    const oRow = oViewModel.getProperty(
                        "/rows/" + row.toString()
                    );
                    const aCells = oRow.cells;

                    // for all cells in the current row
                    for (let c = 0; c < iSizeY; c++) {
                        const cell = aCells[c].iValue;

                        if (cell === 1) {
                            i = i + 1;
                        } else if (cell === 0 && i !== 0) {
                            aTmp.push(i);
                            i = 0;
                        }
                    }

                    // if the last cell was a 1, we didn't add it yet
                    if (i !== 0) {
                        aTmp.push(i);
                    }

                    // write rowHeaders back to model
                    oRow.rowHeader = aTmp;
                    oViewModel.setProperty("/rows/" + row.toString(), oRow);
                }
            },

            _updateCellHeaders: function () {
                const oViewModel = this.getView().getModel("view");
                const iSizeX = oViewModel.getProperty("/x");
                const iSizeY = oViewModel.getProperty("/y");

                const aRows = oViewModel.getProperty("/rows");
                const aColHeads = [];

                // for all columns
                for (let iCol = 0; iCol < iSizeY; iCol++) {
                    let i = 0;
                    const aTmp = [];

                    // for all rows
                    for (let iRow = 0; iRow < iSizeX; iRow++) {
                        const iCell = aRows[iRow].cells[iCol].iValue;

                        if (iCell === 1) {
                            i = i + 1;
                        } else if (iCell === 0 && i !== 0) {
                            aTmp.push(i);
                            i = 0;
                        }
                    }

                    if (i !== 0) {
                        aTmp.push(i);
                    }

                    aColHeads.push(aTmp);
                }
                oViewModel.setProperty("/colHeads", aColHeads);
            },

            onValidatePress: function (oEvent) {
                const oViewModel = this.getView().getModel("view");
                const bValid = this._getUserSolutionValid();

                // show solution if answer is right, but do NOT hide
                // solution if answer is wrong and solution is already visible
                oViewModel.setProperty(
                    "/bShowSolution",
                    oViewModel.getProperty("/bShowSolution") ? true : bValid
                );

                sap.m.MessageToast.show(bValid, {
                    duration: 2000,
                });

                return oEvent;
            },

            _getUserSolutionValid: function () {
                const oViewModel = this.getView().getModel("view");
                const iSizeX = oViewModel.getProperty("/x");
                const iSizeY = oViewModel.getProperty("/y");

                const aRows = oViewModel.getProperty("/rows");
                let bValid = true;

                // for all columns
                column_loop: for (let iCol = 0; iCol < iSizeY; iCol++) {
                    // for all rows
                    for (let iRow = 0; iRow < iSizeX; iRow++) {
                        const oCell = aRows[iRow].cells[iCol];

                        bValid = !!oCell.iValue === oCell.bPressed;

                        if (!bValid) {
                            break column_loop;
                        }
                    }
                }

                return bValid;
            },
        });
    }
);
