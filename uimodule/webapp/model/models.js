sap.ui.define(
    ["sap/ui/model/json/JSONModel", "sap/ui/Device"],
    function (JSONModel, Device) {
        "use strict";

        return {
            createDeviceModel: function () {
                var oModel = new JSONModel(Device);
                oModel.setDefaultBindingMode("OneWay");
                return oModel;
            },

            createViewModel: function () {
                return new JSONModel({
                    x: 5,
                    y: 5,
                    rows: [],
                    colHeads: [],
                });
            },
        };
    }
);
